/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban';
 * console.log(a);
 *
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki
 * nilai akhir yaitu 100
 *
 * Selamat mengerjakan
 */

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/

class Score {
  constructor(pts) {
    this.subject;
    this.points = pts;
    this.email;
  }
  average() {
    let point = this.points;
    if (Array.isArray(point)) {
      let count = point.length;
      let sum = 0;
      for (let i = 0; i < count; i++) {
        sum += point[i];
      }
      return Number(sum / count).toFixed(1);
    }
    return point;
  }
}

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/

const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93],
];

let viewScores = (data, subject) => {
  let email = 0;
  let sbj = 0;
  let val = [];

  switch (subject) {
    case "quiz - 1":
      sbj = 1;
      break;
    case "quiz - 2":
      sbj = 2;
      break;
    case "quiz - 3":
      sbj = 3;
      break;
  }
  for (let i = 1; i < data.length; i++) {
    let Scores = new Score(data[i][sbj]);
    let output = {};
    output.email = data[i][email];
    output.subject = subject;
    output.points = Scores.average();

    val.push(output);
  }
  console.log(val);
};

// TEST CASE
viewScores(data, "quiz-1");
viewScores(data, "quiz-2");
viewScores(data, "quiz-3");

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/

let recapScores = (data) => {
  let output = [];
  for (let n = 1; n < data.length; n++) {
    let arr = [];
    let obj = {};
    let avg = 0;
    for (let i = 1; i < data[n].length; i++) {
      arr[i - 1] = data[n][i];
    }
    let fromScore = new Score(arr);
    avg = fromScore.average();
    if (avg > 0 && avg < 80) {
      obj.predicate = "participant";
    } else if (avg > 80 && avg < 90) {
      obj.predicate = "graduate";
    } else {
      obj.predicate = "honour";
    }
    obj.index = n;
    obj.email = data[n][0];
    obj.average = avg;
    output.push(obj);
  }

  console.log(`Output: `);
  for (let a = 0; a < output.length; a++) {
    console.log(`${output[a].index}. Email: ${output[a].email}
Rata-rata: ${output[a].average} 
Predikat: ${output[a].predicate}`);
    console.log();
  }
};

recapScores(data);
