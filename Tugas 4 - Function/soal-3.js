function introduce(a, b, c, d) {
  return (
    "Nama Saya " +
    a +
    ", umur saya " +
    b +
    " tahun, alamat saya di " +
    c +
    ", dan saya punya hobby yaitu " +
    d
  );
}

var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
