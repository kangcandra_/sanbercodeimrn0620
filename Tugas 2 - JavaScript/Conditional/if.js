var name = "John";
var role;

if (name == null && role == null) {
  console.log("Nama harus diisi!");
} else if (name == null || role == null) {
  console.log("Halo " + name + ", Pilih peranmu untuk memulai game!");
} else if (name == "Jane" && role == "Penyihir") {
  console.log(
    "Selamat datang di Dunia Warewolf, " +
      name +
      "\nHalo Penyihir " +
      name +
      ", kamu dapat melihat siapa yang menjadi Warewolf! "
  );
} else if (name == "Jenita" && role == "Guard") {
  console.log(
    "Selamat datang di Dunia Warewolf, " +
      name +
      "\nHalo Guard " +
      name +
      ", kamu akan membantu melindungi temanmu dari serangan warewolf."
  );
} else if (name == "Junaedi" && role == "Warewolf") {
  console.log(
    "Selamat datang di Dunia Warewolf, " +
      name +
      "\nHalo Warewolf " +
      name +
      ", kamu akan memakan mangsa setiap malam."
  );
}
