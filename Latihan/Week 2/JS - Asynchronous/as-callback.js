// Deklarasi function yang memiliki Callback sebagai parameter
function periksaDokter(nomorAntri, callback) {
  if (nomorAntri > 50) {
    callback(false);
  } else if (nomorAntri < 10) {
    callback(true);
  }
}

// menjalankan function periksaDokter yang sebelumnya sudah dideklarasi
periksaDokter(65, function (check) {
  if (check) {
    console.log("sebentar lagi giliran saya");
  } else {
    console.log("saya jalan-jalan dulu");
  }
});
