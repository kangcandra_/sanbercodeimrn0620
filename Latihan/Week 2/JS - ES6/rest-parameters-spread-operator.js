let scores = ["98", "95", "93", "90", "87", "85"];
let [first, second, third, ...restOfScores] = scores;

console.log(first); // 98
console.log(second); // 95
console.log(third); // 93
console.log(restOfScores); // [90, 87, 85]

let array1 = ["one", "two"];
let array2 = ["three", "four"];
let array3 = ["five", "six"];

// ES5 Way / Normal Javascript
var combinedArray = array1.concat(array2).concat(array3);
console.log(combinedArray); // ['one', 'two', 'three', 'four', 'five', 'six']

// ES6 Way
let combinedArray = [...array1, ...array2, ...array3];
console.log(combinedArray); // ['one', 'two', 'three', 'four', 'five', 'six']
