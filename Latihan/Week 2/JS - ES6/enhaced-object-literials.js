const fullName = "Zell Liew";

const Zell = {
  fullName: fullName,
};

// ES6 way
const Zell = {
  fullName,
};
