// Normal
var x = 1;

if (x === 1) {
  var x = 2;

  console.log(x);
  // expected output: 2
}

console.log(x); // 2

// ES 6
let x = 1;

if (x === 1) {
  let x = 2;

  console.log(x);
  // expected output: 2
}

console.log(x); // 1

const number = 42;
number = 100; // Uncaught TypeError: Assignment to constant variable.
