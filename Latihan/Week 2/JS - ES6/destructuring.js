// Normal
let studentName = {
  firstName: "Peter",
  lastName: "Parker",
};

const firstName = studentName.firstName;
const lastName = studentName.lastName;

// ES 6
let studentName = {
  firstName: "Peter",
  lastName: "Parker",
};

const { firstName, lastName } = studentName;

console.log(firstName); // Peter
console.log(lastName); // Parker s
