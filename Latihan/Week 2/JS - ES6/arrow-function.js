// Normal
function f() {
  // isi Function
}

var f = function () {
  // isi function
};

// ES 6
var f = () => {
  //function
  return () => {
    //returning a function
  };
};
