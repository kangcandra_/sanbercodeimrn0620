// Static Method
// Static methods didefinisikan hanya untuk class itu sendiri.
// sehingga, jika melihat pada contoh sebelumnya static method hanya bisa diakses melalui Car,
// dan tidak bisa melalui mycar:

class Car {
  constructor(brand) {
    this.carname = brand;
  }

  static hello() {
    return "Hello";
  }
}

mycar = new Car("Ford"); // memanggil 'hello()' pada class Car:
console.log(Car.hello());

// dan tidak bisa pada 'mycar':
// console.log(mycar.hello());
// jika menggunakan sintaks tersebut akan memunculkan error.
// console.log(Car.hello());
