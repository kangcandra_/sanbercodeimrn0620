class Car {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
    this.sound = "honk! honk!vroomvroom";
  }
}

console.log(Car.name);
