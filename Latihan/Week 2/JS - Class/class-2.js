// Tidak diberi nama
var Car = class {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};

console.log(Car.name);

// diberi nama
var Car = class Car2 {
  constructor(brand, factory) {
    this.brand = brand;
    this.factory = factory;
  }
};
console.log(Car.name);
