class Car {
  constructor(brand) {
    this.carname = brand;
  }

  present() {
    return "i have a " + this.carname;
  }
}

mycar = new Car("Ford");
console.log(mycar.present());
