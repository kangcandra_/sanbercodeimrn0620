// Strict Equal ( === ) Selain membandingkan dua nilai nya,
// strict equal juga membandingkan tipe datanya apakah sama atau tidak

var angka = 8;
console.log(angka == "8"); // true, padahal "8" adalah string.
console.log(angka === "8"); // false, karena tipe data nya berbeda
console.log(angka === 8); // true
