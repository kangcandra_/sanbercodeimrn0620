// Strict not Equal( !== ) Kebalikan dari strict equal.
var angka = 11;
console.log(angka != "11"); // false, padahal "11" adalah string
console.log(angka !== "11"); // true, karena tipe datanya berbeda
console.log(angka !== 11); // false
