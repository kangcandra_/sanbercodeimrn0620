// example 1
if (true) {
  console.log("jalankan code");
}

// example 2
if (false) {
  console.log("code tidak dijalankan");
}

// example 3
var mood = "happy";
if (mood == "happy") {
  console.log("hari ini aku bahagia!");
}
