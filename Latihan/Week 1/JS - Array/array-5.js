// .shift()
// Shift kebalikan dari unshift yaitu menghapus nilai pada elemen terdepan
// dari sebuah array. metode Shift tidak menerima parameter apapun.

var numbers = [0, 1, 2, 3];
numbers.shift();
console.log(numbers);