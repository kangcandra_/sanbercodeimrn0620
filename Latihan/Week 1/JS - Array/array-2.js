// .push()
// Push adalah metode array untuk menambahkan nilai di belakang elemen terakhir di array.metode push menerima sebuah parameter yaitu nilai yang ingin kita tambahkan ke dalam array.

var numbers = [0, 1, 2]
numbers.push(3)
console.log(numbers)
numbers.push(4, 5)
console.log(numbers)  