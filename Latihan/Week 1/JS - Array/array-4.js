// .unshift()
// Unshift yaitu menambahkan nilai pada index ke - 0
// sehingga elemen - elemen sebelumnya bergeser.

var numbers = [0, 1, 2, 3];

numbers.unshift(-1);
console.log(numbers);
