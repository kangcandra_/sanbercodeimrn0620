// .splice()
// Splice yaitu metode untuk menghapus dan / atau menambahkan
// nilai elemen pada array.
// Metode splice bisa menerima parameter sebanyak dua atau lebih parameter.
// Jika ingin menggunakan splice untuk menghapus elemen pada index tertentu
// maka digunakan 2 paramater.
// Jika ingin menggunakan splice untuk menambahkan elemen pada index tertentu
// maka digunakan tiga parameter.

var fruits = ["banana", "orange", "grape"]
fruits.splice(1, 0, "watermelon")
console.log(fruits)

var fruits = ["banana", "orange", "grape"]
fruits.splice(0, 2)
console.log(fruits) 