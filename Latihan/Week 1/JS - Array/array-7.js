// .slice()
// slice adalah metode untuk mengambil irisan dari sebuah array.
// Metode slice bisa menerima satu atau dua parameter.
// Parameter pertama adalah nomer index pertama yang akan kita ambil sebagai irisan,
// sedangkan parameter kedua adalah nomer index terakhir
// yang ingin kita ambil sebagai irisan.

var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1, 3)
console.log(irisan1)
var irisan2 = angka.slice(0, 2)
console.log(irisan2)

var angka = [0, 1, 2, 3]
var irisan3 = angka.slice(2)
console.log(irisan3)

var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()
console.log(salinAngka) 