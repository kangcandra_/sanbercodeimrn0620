// .split() dan.join()
// Metode split yaitu memecah sebuah string sehingga menjadi sebuah array.
// Split menerima sebuah parameter berupa karakter yang menjadi separator
// untuk memecah string.

var biodata = "name:john,doe"
var name = biodata.split(":")
console.log(name) 