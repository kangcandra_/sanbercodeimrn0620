// .pop()
// Pop adalah kebalikan dari push yaitu menghapus nilai elemen paling terakhir
// dari sebuah array.metode Pop tidak menerima parameter apapun
// sehingga metode pop hanya bisa mengeluarkan satu elemen saja
// yaitu yang paling terakhir dari sebuah array.

var numbers = [0, 1, 2, 3, 4, 5];
numbers.pop();
console.log(numbers);