// .substr([indeks awal], [jumlah karakter yang diambil(optional)])
// Mendapatkan potongan string mulai dari indeks pada parameter pertama(indeks awal) dengan jumlah
// indeks pada parameter kedua(jumlah karakter).Bila parameter kedua tidak ditentukan,
// maka secara otomatis berakhir pada karakter terakhir.Karakter pada indeks yang ditentukan pada
// parameter kedua tidak diikutkan sebagai output.

var motor1 = "Zelda Motor";
var motor2 = motor1.substr(2, 2);
console.log(motor2);
