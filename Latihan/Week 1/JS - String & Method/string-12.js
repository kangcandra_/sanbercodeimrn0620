// parseInt([String]) dan parseFloat([String])

// Fungsi global parseInt([String]) dan parseFloat([String]) mengembalikan angka dari string.
// Bila angka adalah bilangan desimal maka gunakan parseFloat(), bila tidak bilangan
// dibelakang koma akan diabaikan.

var int = "89";
var real = "56.7";
var StrInt_1 = parseInt(int);
var StrInt_2 = parseInt(real);
var StrReal_1 = parseFloat(int);
var StrReal_2 = parseFloat(real);

console.log(StrInt_1);
console.log(StrInt_2);
console.log(StrReal_1);
console.log(StrReal_2);
