// Number([String])

// Fungsi global Number() mengonversi tipe data string menjadi angka.Data yang diberikan
// pada parameter harus berupa karakter angka saja, dengan titik(separator) bila angka adalah
// bilangan desimal.Bila parameter berisi karakter selain angka dan / atau titik,
// Number() akan mengembalikan NaN(Not a Number).

var number1 = Number("90");
var number2 = Number("1.23");
var number3 = Number("4 5");

console.log(number1);
console.log(number2);
console.log(number3);
