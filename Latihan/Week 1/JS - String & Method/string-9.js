// .trim()

// Mengembalikan string baru yang sudah dihapus karakter whitespace(” “)
// pada awal dan akhir string tersebut.

var username = " admin ";
var newUsername = username.trim();
console.log(newUsername);
