// .toString()

// Mengonversi tipe data lain menjadi string.Bila data tersebut adalah array,
// setiap nilai akan dituliskan dan dipisah dengan karakter koma.

var number = 21;
console.log(number.toString());
var arr = [1, 2];
console.log(arr.toString());
