// .substring([indeks awal], [indeks akhir(optional)])

// Mengembalikan potongan string mulai dari indeks pada parameter pertama(indeks awal) sampai dengan indeks pada parameter kedua(indeks akhir).Bila parameter kedua tidak ditentukan,
// maka secara otomatis berakhir pada karakter terakhir.Karakter pada indeks yang ditentukan pada
// parameter kedua tidak diikutkan sebagai output.

var car1 = "Lykan Hypersport";
var car2 = car1.substr(6);
console.log(car2);
