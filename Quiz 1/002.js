function DescendingTen(num) {
  var a = num - 10;
  var value = [];
  if (num == null) {
    return -1;
  } else if (num) {
    for (i = num; i > a; i--) {
      value.push(i);
    }
  }
  return value;
}

function AscendingTen(num) {
  var a = num + 10;
  var value = [];
  if (num == null) {
    return -1;
  } else if (num) {
    for (i = num; i < a; i++) {
      value.push(i);
    }
  }
  return value;
}

function ConditionalAscDesc(reference, check) {
  if (check % 2 == 1) {
    var a = reference + 10;
    var value = [];
    if (reference == null || check == null) {
      return -1;
    } else if (reference) {
      for (i = reference; i < a; i++) {
        value.push(i);
      }
    }
  } else {
    var a = reference - 10;
    var value = [];
    if (reference == null || check == null) {
      return -1;
    } else if (reference) {
      for (i = reference; i > a; i--) {
        value.push(i);
      }
    }
  }
  return value;
}

function ularTangga() {
  var z = "";
  var x = 100;
  for (var a = 1; a <= 10; a++) {
    if (a % 2 == 0) {
      z = "";
      for (var b = x - 9; b <= x; b++) {
        z += b + " ";
      }
      console.log(z);
      x = b - 11;
    } else {
      z = "";
      for (var ai = x; ai >= x - 9; ai--) {
        z += ai + " ";
      }
      console.log(z);
      x = ai;
    }
  }
  return "";
}

console.log("===================== A.Descending Ten =====================");
console.log(DescendingTen(100));
console.log(DescendingTen(10));
console.log(DescendingTen());
console.log("==================== End Descending Ten ====================\n");

// TEST CASES Ascending Ten
console.log("===================== B.Ascending Ten =====================");
console.log(AscendingTen(11));
console.log(AscendingTen(21));
console.log(AscendingTen());
console.log("==================== End Ascending Ten ====================\n");

console.log(
  "===================== c.Conditional AscDesc Ten ====================="
);
console.log(ConditionalAscDesc(20, 8));
console.log(ConditionalAscDesc(81, 1));
console.log(ConditionalAscDesc(31));
console.log(ConditionalAscDesc());
console.log(
  "==================== End Conditional AscDesc ====================\n"
);
console.log("===================== d.Snake and Ladders =====================");
console.log(ularTangga());
console.log("==================== End Snake and Ladders ====================");
