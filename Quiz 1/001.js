function bandingkan(num1, num2) {
  if (num1 == num2) {
    return -1;
  } else if (num1 < 0 && num2 < 0) {
    return -1;
  } else if (num1 < num2) {
    return num2;
  } else if (num1 == null || num2 == null) {
    return 1;
  }
}

function balikString(str) {
  var currentString = str;
  var newString = "";
  for (let i = str.length - 1; i >= 0; i--) {
    newString = newString + currentString[i];
  }

  return newString;
}
function palindrome(str) {
  var currentString = str;
  var newString = "";
  for (let i = str.length - 1; i >= 0; i--) {
    newString = newString + currentString[i];
  }
  if (newString == str) {
    return true;
  } else {
    return false;
  }
}

// TEST CASES Bandingkan Angka
console.log("===================== A.Bandingkan Angka =====================");
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1
console.log(bandingkan(112, 121)); // 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")); // 18
console.log("==================== End Bandingkan Angka ====================\n");

// TEST CASES BalikString
console.log("===================== B.Balik String =====================");
console.log(balikString("abcde")); // edcba
console.log(balikString("rusak")); // kasur
console.log(balikString("racecar")); // racecar
console.log(balikString("haji")); // ijah
console.log("==================== End Balik String ====================\n");

// TEST CASES Palindrome
console.log("===================== C.Palindrome =====================");
console.log(palindrome("kasur rusak")); // true
console.log(palindrome("haji ijah")); // true
console.log(palindrome("nabasan")); // false
console.log(palindrome("nababan")); // true
console.log(palindrome("jakarta")); // false
console.log("==================== End Palindrome ====================\n");
