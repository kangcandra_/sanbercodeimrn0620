
function rangeWithStep(startNum, finishNum, step) {
    var arr = [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i += step) {
            arr.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i -= step) {
            arr.push(i);
        }
    }
    return arr;
}

console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

