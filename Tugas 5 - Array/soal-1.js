
function range(startNum, finishNum) {
    var arr = [];
    if (startNum < finishNum) {
        for (var i = startNum; i <= finishNum; i++) {
            arr.push(i);
        }
    } else if (startNum > finishNum) {
        for (var i = startNum; i >= finishNum; i--) {
            arr.push(i);
        }
    } else if (startNum == null || finishNum == null) {
        return -1
    }
    return arr;
}

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

