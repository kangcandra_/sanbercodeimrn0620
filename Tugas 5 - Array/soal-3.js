function sum(startNum, finishNum, step = 1) {
  var sum = 0;
  if (startNum < finishNum) {
    for (var i = startNum; i <= finishNum; i += step) {
      sum = sum + i;
    }
  } else if (startNum > finishNum) {
    for (var i = startNum; i >= finishNum; i -= step) {
      sum = sum + i;
    }
  } else if (startNum == null && finishNum == null) {
    sum = 0;
  } else {
    sum = 1;
  }
  return sum;
}

console.log(sum(1, 10));
console.log(sum(5, 50, 2));
console.log(sum(15, 10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());
