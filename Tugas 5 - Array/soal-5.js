function balikKata(str) {
  var currentString = str;
  var newString = "";
  for (let i = str.length - 1; i >= 0; i--) {
    newString = newString + currentString[i];
  }

  return newString;
}

console.log(balikKata("Kasur Rusak"));
console.log(balikKata("SanberCode"));
console.log(balikKata("Haji Ijah"));
console.log(balikKata("racecar"));
console.log(balikKata("I am Sanbers"));
