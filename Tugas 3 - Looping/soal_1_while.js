var a = 2;
var b = 20;
console.log("Looping Pertama");

while (a <= 20) {
  console.log(a + " - I Love Coding");
  a += 2;
}

console.log("Looping Kedua");

while (b > 2) {
  b -= 2;
  console.log(b + " - I will become a Mobile Developer");
}
