class Animal {
  constructor(name, legs = 4, cold_blooded = false) {
    this._name = name;
    this._legs = legs;
    this._cold_blooded = cold_blooded;
  }
}

class Ape extends Animal {
  constructor() {
    super()._legs = 2;
  }
  yell() {
    console.log("Auoo");
  }
}
class Frog extends Animal {
  constructor() {
    super();
  }
  jump() {
    console.log("hop hop");
  }
}

var sheep = new Animal("shaun");
console.log(sheep._name);
console.log(sheep._legs);
console.log(sheep._cold_blooded);

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"
