function arrayToObject(param) {
  var now = new Date();
  var thisYear = now.getFullYear();
  var age = "";

  for (var i = 0; i < param.length; i++) {
    for (var a = 0; a < param[i].length; a++) {
      if (param[i][3] > thisYear || param[i][3] == undefined) {
        age = "Invalid Birthday";
      } else {
        age = thisYear - param[i][3];
      }
    }
    var object = {};
    object.firstName = param[i][0];
    object.lastName = param[i][1];
    object.gender = param[i][2];
    object.age = age;
    console.log(i + 1 + " ." + param[i][0] + " " + param[i][1], object);
  }
}

var people = [
  ["Bruce", "Banner", "male", 1975],
  ["Natasha", "Romanoff", "female"],
];
arrayToObject(people);
var people2 = [
  ["Tony", "Stark", "male", 1980],
  ["Pepper", "Pots", "female", 2023],
];
arrayToObject(people2);
