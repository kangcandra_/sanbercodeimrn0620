var products = [
  ["Sepatu brand Stacattu", 1500000],
  ["Sweater brand Uniklooh", 175000],
  ["Baju brand Zoro", 500000],
  ["Baju brand H&N", 250000],
  ["Casing Handphone", 50000],
];

function shoppingTime(memberId, money) {
  products = products.sort(function (a, b) {
    return b[1] - a[1];
  });

  var changeMoney = 0;
  var buyProducts = [];
  var listProduct = {};

  if (memberId != null && money != null) {
    if (money >= 50000) {
      changeMoney = money;
      for (var x = 0; x < products.length; x++) {
        if (changeMoney >= products[x][1]) {
          changeMoney -= products[x][1];
          buyProducts.push(products[x][0]);
        }
      }
      listProduct.memberId = memberId;
      listProduct.money = money;
      listProduct.listPurchased = buyProducts;
      listProduct.changeMoney = changeMoney;

      return listProduct;
    } else {
      return "Mohon Maaf, uang tidak cukup";
    }
  } else {
    return "Mohon maaf, toko X hanya berlaku untuk member saja";
  }
}

console.log(shoppingTime("1820RzKrnWn08", 2475000));
console.log(shoppingTime("82Ku8Ma742", 170000));
console.log(shoppingTime("", 2475000));
console.log(shoppingTime("234JdhweRxa53", 15000));
console.log(shoppingTime());
