var rute = ["A", "B", "C", "D", "E", "F"];
rute = rute.sort(function (a, b) {
  return a < b ? -1 : 1;
});

function naikAngkot(listPenumpang) {
  var output = [];
  var takeAngkot = 0;
  var aim = 0;
  var indexTakeAngkot = 0;
  var indexAim = 0;
  var total = 0;

  if (listPenumpang !== undefined || listPenumpang.length != 0) {
    for (let x = 0; x < listPenumpang.length; x++) {
      takeAngkot = listPenumpang[x][1];

      aim = listPenumpang[x][2];

      for (let index = 0; index < rute.length; index++) {
        if (takeAngkot == rute[index]) {
          indexTakeAngkot = index + 1;
        }
        if (aim == rute[index]) {
          indexAim = index + 1;
        }
      }
      var listAngkot = {};
      total = (indexAim - indexTakeAngkot) * 2000;
      listAngkot.penumpang = listPenumpang[x][0];
      listAngkot.naikDari = takeAngkot;
      listAngkot.aim = aim;
      listAngkot.bayar = total;
      output.push(listAngkot);
    }
  }
  return output;
}

console.log(
  naikAngkot([
    ["Dimitri", "B", "F"],
    ["Icha", "A", "B"],
  ])
);
console.log(naikAngkot([]));
